/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

/**
 *
 * @author Mateusz
 */
public class Listener implements VetoableChangeListener{
    private ConsultationListBean consultationList; 
    
    public Listener(ConsultationListBean consultationList) {
        this.consultationList = consultationList;
    } 
    
    @Override
    public void vetoableChange(PropertyChangeEvent pce) throws PropertyVetoException {
        if (pce.getPropertyName().equals("Term")) { 
            TermBean stary = (TermBean)pce.getOldValue();
            TermBean nowy = (TermBean)pce.getNewValue();
        
        for(int i=0; i < this.consultationList.getSize() - 1; i++) { 
            if(this.consultationList.getConsultation(i) == stary) {
                continue;
            }   
            if(!consultationList.getConsultation(i).getBeginDate().after(nowy.getEnd()) && !consultationList.getConsultation(i).getEndDate().before(nowy.getBegin())) { 
                    throw new PropertyVetoException("Listener PropertyVetoException", null); 
                }
            }
        }
    }
    
}
