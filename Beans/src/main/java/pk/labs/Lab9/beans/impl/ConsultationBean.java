/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;
import java.util.Date;
import pk.labs.Lab9.beans.Term;

/**
 *
 * @author Mateusz
 */
public class ConsultationBean implements Serializable, pk.labs.Lab9.beans.Consultation{
    private String student;
    private Term term;
    
    private final VetoableChangeSupport veto = new VetoableChangeSupport(this);
    private final PropertyChangeSupport property = new PropertyChangeSupport(this);

    public ConsultationBean() { }
    
    @Override
    public String getStudent() {
        return this.student;
    }

    @Override
    public void setStudent(String student) {
        this.student = student;
    }

    @Override
    public Date getBeginDate() {
        return this.term.getBegin();
    }

    @Override
    public Date getEndDate() {
        return this.term.getEnd();
    }

    @Override
    public void setTerm(Term term) throws PropertyVetoException {
        Term oldTerm = this.term;
        veto.fireVetoableChange("Term", oldTerm, term);
        this.term = term;
        property.firePropertyChange("Term", oldTerm, term);
    }
    
    public Term getTerm() {
        return this.term;
    }

    @Override
    public void prolong(int minutes) throws PropertyVetoException {
         if (minutes <= 0) {
            minutes = 0;
        }

        Term newTerm = new TermBean();
        newTerm.setBegin(this.term.getBegin());
        newTerm.setDuration(this.term.getDuration() + minutes);
        int oldDuration = this.term.getDuration();
        this.veto.fireVetoableChange("Term", term, newTerm);
        this.term.setDuration(this.term.getDuration() + minutes);
        this.property.firePropertyChange("Term", oldDuration, oldDuration + minutes);
    }
    
    public void addVeto(Listener listener) {
        this.veto.addVetoableChangeListener(listener);
    }
}
