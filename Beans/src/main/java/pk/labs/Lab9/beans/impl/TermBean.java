/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Mateusz
 */
public class TermBean implements Serializable, pk.labs.Lab9.beans.Term{
    
    Date begin;
    int duration;
    Date end;
   
    @Override
    public Date getBegin() {
        return this.begin;
    }
    
    @Override
    public void setBegin(Date begin) {
        this.begin = begin;
    }
    
    @Override
    public int getDuration() {
       return this.duration;
    }
 
    @Override
    public void setDuration(int duration) {
        if (duration > 0) {
            this.duration = duration;
        }
    }
    
   @Override
    public Date getEnd() {
        return this.end = new Date(begin.getTime() + this.duration * 60L * 1000);
    }
    
}
