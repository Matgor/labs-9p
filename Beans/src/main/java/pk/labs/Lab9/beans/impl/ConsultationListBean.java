/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pk.labs.Lab9.beans.impl;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.io.Serializable;
import java.util.Arrays;
import pk.labs.Lab9.beans.Consultation;

/**
 *
 * @author Mateusz
 */
public class ConsultationListBean implements Serializable, pk.labs.Lab9.beans.ConsultationList{
    
    private PropertyChangeSupport zmiany = new PropertyChangeSupport(this);
    private Listener veto = new Listener(this);
    private Consultation[] lista;

    public ConsultationListBean() {
        this.lista = new Consultation[]{};
    }
      
    @Override
    public int getSize() {
        return this.lista.length;
    }
   
    @Override
    public Consultation[] getConsultation() {
        return this.lista;
    }
    
    @Override
    public Consultation getConsultation(int index) {
        return this.lista[index];
    }
   
    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        for (int i = 0; i < this.lista.length; i++) {
            if ((consultation.getEndDate().after(this.lista[i].getBeginDate()) && consultation.getBeginDate().before(this.lista[i].getBeginDate()))
                    || (consultation.getBeginDate().before(this.lista[i].getEndDate()) && consultation.getEndDate().after(this.lista[i].getBeginDate()))) {
                throw new PropertyVetoException("ConsultationListBean PropertyVetoException", null);
            }
        }

        Consultation[] nowe = Arrays.copyOf(this.lista, this.lista.length + 1);
        Consultation[] stare = this.lista;
        nowe[nowe.length - 1] = consultation;
        ((ConsultationBean) consultation).addVeto(veto);
        this.lista = nowe;
        zmiany.firePropertyChange("consultation", stare, nowe);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        this.zmiany.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        this.zmiany.addPropertyChangeListener(listener);
    }
    
}
